#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"
#include "cPlayer.h"

#include <iostream>

bool isShiftKeyDown( int mods, bool bByItself = true );
bool isCtrlKeyDown( int mods, bool bByItself = true );
bool isAltKeyDown( int mods, bool bByItself = true );

extern int g_selectedGameObjectIndex;
extern int g_selectedLightIndex;
extern bool g_movingGameObject;
extern bool g_lightsOn;
extern bool g_texturesOn;
extern bool g_movingLights;
extern bool g_boundingBoxes;
extern cCamera* g_pTheCamera;
extern int selectedLanguage;
extern glm::vec3 cameraOffset;
extern float playerSpeed;

const float MOVESPEED = 0.9f;
const float CAMERAROTATIONSPEED = 1;
const float CAMERASPEED = 2;

extern cPlayer* thePlayer;

void updateCamera(glm::vec3 turnChange)
{
	g_pTheCamera->adjustQOrientationFormDeltaEuler(turnChange);

	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->qCameraOrientation) *
		glm::vec4(cameraOffset.x, cameraOffset.y, cameraOffset.z, 1);


	glm::vec3 objectPos;
	objectPos = g_pTheCamera->theObject->position;
	glm::vec3 position = glm::vec3(tempPos.x + objectPos.x
		, tempPos.y + objectPos.y,
		tempPos.z + objectPos.z);

	g_pTheCamera->eye = position;
}

void updateCamera()
{
	glm::vec3 position = glm::vec3(cameraOffset.x + g_pTheCamera->target.x
		, cameraOffset.y + g_pTheCamera->target.y,
		cameraOffset.z + g_pTheCamera->target.z);

	g_pTheCamera->eye = position;
}




/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

	cGameObject* pLeftTeapot = findObjectByFriendlyName(LEFTTEAPOTNAME, ::g_vecGameObjects);

	switch (key)
	{
	case GLFW_KEY_1: //Move the camera "forward"
	{
		if (action == GLFW_PRESS)
			selectedLanguage = 0;
	}
	break;
	case GLFW_KEY_2: //Move the camera "forward"
	{
		if (action == GLFW_PRESS)
			selectedLanguage = 1;
	}
	break;
	case GLFW_KEY_3: //Move the camera "forward"
	{
		if (action == GLFW_PRESS)
			selectedLanguage = 2;
	}
	break;
	case GLFW_KEY_4: //Move the camera "forward"
	{
		if (action == GLFW_PRESS)
			selectedLanguage = 3;
	}
	break;
	case GLFW_KEY_5: //Move the camera "forward"
	{
		if (action == GLFW_PRESS)
			selectedLanguage = 4;
	}
	break;
	case GLFW_KEY_UP: //Move the camera "forward"
	{
		g_pTheCamera->zDist += 0.5;
		g_pTheCamera->updateTick(0);
		//updateCamera(glm::vec3(0, 0, 0));
	}
	break;
	case GLFW_KEY_DOWN: //Move the camera "backward"
	{
		g_pTheCamera->zDist -= 0.5;
		g_pTheCamera->updateTick(0);
		//updateCamera(glm::vec3(0, 0, 0));
	}
	break;
	case GLFW_KEY_LEFT: //Move the camera "backward"
	{
		updateCamera(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
	}
	break;
	case GLFW_KEY_RIGHT: //Move the camera "backward"
	{
		updateCamera(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
	}
	break;

	case GLFW_KEY_D:
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
		{
			thePlayer->movingLeft = true;
			//thePlayer->vel.x = playerSpeed;
		}
		else if (action == GLFW_RELEASE)
		{
			thePlayer->movingLeft = false;
			//if (::g_vecGameObjects[0]->vel.x > 0)
			//{
			//	::g_vecGameObjects[0]->vel.x = 0;
			//}
		}
	}
	break;

	case GLFW_KEY_A: 
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
		{
			thePlayer->movingRight= true;
			//::g_vecGameObjects[0]->vel.x = -playerSpeed;
		}
		else if (action == GLFW_RELEASE)
		{
			thePlayer->movingRight = false;
			//if (::g_vecGameObjects[0]->vel.x < 0)
			//{
			//	::g_vecGameObjects[0]->vel.x = 0;
			//}
		}
	}
	break;

	case GLFW_KEY_S:
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
		{
			thePlayer->movingDown = true;
			//::g_vecGameObjects[0]->vel.z = playerSpeed;
		}
		else if (action == GLFW_RELEASE)
		{
			thePlayer->movingDown = false;
			//if (::g_vecGameObjects[0]->vel.z > 0)
			//{
			//	::g_vecGameObjects[0]->vel.z = 0;
			//}
		}
	}
	break;

	case GLFW_KEY_W:
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
		{
			thePlayer->movingUp = true;
			//::g_vecGameObjects[0]->vel.z = -playerSpeed;
		}
		else if (action == GLFW_RELEASE)
		{
			thePlayer->movingUp = false;
			//if (::g_vecGameObjects[0]->vel.z < 0)
			//{
			//	::g_vecGameObjects[0]->vel.z = 0;
			//}
		}
	}
	break;


	}
	// HACK: print output to the console
//	std::cout << "Light[0] linear atten: "
//		<< ::g_pLightManager->vecLights[0].attenuation.y << ", "
//		<< ::g_pLightManager->vecLights[0].attenuation.z << std::endl;
	return;
}



// Helper functions
bool isShiftKeyDown( int mods, bool bByItself /*=true*/ )
{
	if ( bByItself )
	{	// shift by itself?
		if ( mods == GLFW_MOD_SHIFT )	{ return true; }
		else							{ return false; }
	}
	else
	{	// shift with anything else, too
		if ( ( mods && GLFW_MOD_SHIFT ) == GLFW_MOD_SHIFT )	{	return true;	}
		else												{	return false;	}
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

//bool isCtrlKeyDown( int mods, bool bByItself = true );
//bool isAltKeyDown( int mods, bool bByItself = true );