#include "cPlayer.h"
//extern std::vector< cGameObject* >  g_vecGameObjects;
extern std::vector< cGameObject* >  enemies;
extern float HITDISTANCE;
extern float playerSpeed;


cPlayer::cPlayer(int healthIn)
{
	health = healthIn;
}

void cPlayer::MakeDecisions()
{
	int size = enemies.size();
	if (health < 0)
	{
		diffuseColour = glm::vec4(1, 1, 1, 1);
		usingTextures = false;
	}
	else
	{
		usingTextures = true;
		for (int j = 1; j < size; ++j)
		{
			if (glm::distance(position, enemies[j]->position) < HITDISTANCE && enemies[j]->bIsUpdatedInPhysics)
			{
				health -= 1;
				diffuseColour = glm::vec4(1, 0, 0, 1);
				usingTextures = false;
			}
		}
	}

	if (movingLeft)
		vel.x = playerSpeed;
	if (movingRight)
		vel.x = -playerSpeed;
	if (movingDown)
		vel.z = playerSpeed;
	if (movingUp)
		vel.z = -playerSpeed;
}
