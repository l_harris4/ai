#ifndef _cCautiousEnemy_HG_
#define _cCautiousEnemy_HG_
#include "cGameObject.h"

class cCautiousEnemy : public cGameObject
{
public:
	virtual void MakeDecisions();
};

#endif
