#include "cAngryEnemy.h"
#include "cPlayer.h"
extern cPlayer* thePlayer;
extern float DETECTIONDISTANCE;
extern float opponentSpeed;

void cAngryEnemy::MakeDecisions()
{
	//if aggresive enemy
	//if player isn't low health and distance to player is less then detection distance then check where the player is looking
	if (thePlayer->health > 50 && glm::distance(position, thePlayer->position) < DETECTIONDISTANCE)
	{
		glm::vec3 playerToEnemy = position - thePlayer->position;
		// the player's forward vector
		glm::vec3 playerForward = thePlayer->qOrientation * glm::vec3(0.f, 0.f, 1.f);

		float dot = glm::dot(playerToEnemy, playerForward);
		//if the player is looking at me, then give velocity away 
		if (dot > 0)
		{
			glm::vec3 force = glm::normalize(position - thePlayer->position) * opponentSpeed;

			vel += force;

			diffuseColour = glm::vec4(1, 1, 0, 1);
			usingTextures = false;
		}
		else  //else give velocity toward
		{
			glm::vec3 force = glm::normalize(thePlayer->position - position) * opponentSpeed;

			if (glm::distance(thePlayer->position, position) > 0.3)
				vel += force;
			else
				vel = glm::vec3(0);

			diffuseColour = glm::vec4(1, 0, 0, 1);
			usingTextures = false;
		}
	}
	else if (thePlayer->health <= 50)//else if health is low move toward the player
	{
		glm::vec3 force = glm::normalize(thePlayer->position - position) * opponentSpeed;

		if (glm::distance(thePlayer->position, position) > 0.5)
			vel += force;
		else
			vel = glm::vec3(0);

		diffuseColour = glm::vec4(1, 0, 0, 1);
		usingTextures = false;
	}
	else
	{
		usingTextures = true;
	}
}
