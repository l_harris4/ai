#ifndef _cGameObject_HG_
#define _cGameObject_HG_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <string>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <vector>
#include "sMeshDrawInfo.h"


enum eTypeOfObject
{	// Ok people, here's the deal:
	SPHERE = 0,		// - it's a sphere
	PLANE = 1,		// - it's a plane
	CAPSULE = 2,    // - it's a capsule
	AABB = 3,		// 3- it's an axis-aligned box
	PLAYER = 4,
	ANGRYENEMY = 5,
	CAUTIOUSENEMY = 6,
	FOLLOWERENEMY = 7,
	UNKNOWN = 99	// I don't know
};

class cGameObject
{
public:
	cGameObject();		// constructor
	~cGameObject();		// destructor
	virtual void MakeDecisions();

	glm::vec3 position;
	glm::quat qOrientation;
	float scale;
	glm::vec3 vel;
	glm::vec3 accel;
	bool bIsUpdatedInPhysics;
	bool bDiscardTexture = false;
	bool bTransparentTexture = false;
	bool isSkybox = false;
	bool usingTextures = true;
	eTypeOfObject typeOfObject;
	int followObjectId = 0;
	float radius;
	bool bIsVisible = true;
	std::vector<sMeshDrawInfo> vecMeshes;
	glm::quat getFinalMeshQOrientation(unsigned int meshID);
	glm::quat getFinalMeshQOrientation(glm::quat &meshQOrientation);
	std::vector< cGameObject* > vec_pChildObjects;
	void DeleteChildren(void);
	cGameObject* FindChildByFriendlyName(std::string name);
	cGameObject* FindChildByID(unsigned int ID);
	bool bIsWireFrame;
	glm::vec4 diffuseColour;
	std::string meshName;
	static const unsigned int NUMTEXTURES = 10;
	std::string textureNames[NUMTEXTURES];
	float textureBlend[NUMTEXTURES];
	std::string friendlyName;
	inline unsigned int getUniqueID(void) { return this->m_UniqueID; }
	void overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation);
	void adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange);
private:
	unsigned int m_UniqueID;
	static unsigned int m_nextUniqueID;

};

#endif
