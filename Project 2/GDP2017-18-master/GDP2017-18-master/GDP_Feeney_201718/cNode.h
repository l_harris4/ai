#pragma once
#ifndef _CNODE_HG_
#define _CNODE_HG_


#include <vector>
#include <string>

class cNode
{
public:
	cNode();
	cNode(std::string id);
	~cNode();
	std::string Id;
	std::vector<cNode*> connectedNodes;
	int xLoc;
	int zLoc;
	int lowestJumpNum;
	std::string shortestPath;
	bool isValid;
	int gValue;
	int hValue;
	int fValue;

	void AddConnection(cNode& nodeToAdd);
	float DistanceToNode(cNode& nodeComparing);
	void findPathTo(std::string ID, cNode* currentNode, std::string IDsInPath = "", int currentJumpNum = 0);

};

#endif // _CNODE_HG_