#include "cCautiousEnemy.h"
#include "cPlayer.h"
extern cPlayer* thePlayer;
extern float DETECTIONDISTANCE;
extern float opponentSpeed;
extern float CAUTIOUSSTOPDISTANCE;

void cCautiousEnemy::MakeDecisions()
{
	//if curious enemy
	//if player is within detection
	if (glm::distance(position, thePlayer->position) < DETECTIONDISTANCE)
	{
		//if player not looking and distance between you and player isnt too much, move in more
		glm::vec3 playerToEnemy = position - thePlayer->position;
		// the player's forward vector
		glm::vec3 playerForward = thePlayer->qOrientation * glm::vec3(0.f, 0.f, 1.f);

		float dot = glm::dot(playerToEnemy, playerForward);
		//if the player is looking at me, then give velocity away 
		if (dot > 0)
		{
			glm::vec3 force = glm::normalize(position - thePlayer->position) * opponentSpeed;

			vel += force;
			diffuseColour = glm::vec4(0, 1, 1, 1);
			usingTextures = false;
		}
		else  //else give velocity toward
		{
			glm::vec3 force = glm::normalize(thePlayer->position - position) * opponentSpeed;

			if (glm::distance(thePlayer->position, position) > CAUTIOUSSTOPDISTANCE)
			{
				vel += force;
				diffuseColour = glm::vec4(0, 0, 1, 1);
				usingTextures = false;
			}
			else
			{
				vel = glm::vec3(0);
				usingTextures = true;
			}
		}
		//if the player is looking at you run
	}
	else
	{
		usingTextures = true;
	}
}
