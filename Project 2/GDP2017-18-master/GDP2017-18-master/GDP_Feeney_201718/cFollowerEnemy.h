#ifndef _cFollowerEnemy_HG_
#define _cFollowerEnemy_HG_
#include "cGameObject.h"

class cFollowerEnemy : public cGameObject
{
public:
	virtual void MakeDecisions();
};

#endif
