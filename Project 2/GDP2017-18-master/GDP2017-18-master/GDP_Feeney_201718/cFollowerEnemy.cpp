#include "cFollowerEnemy.h"
#include "cPlayer.h"
extern cPlayer* thePlayer;
extern float DETECTIONDISTANCE;
extern float opponentSpeed;
extern std::vector< cGameObject* >  enemies;

void cFollowerEnemy::MakeDecisions()
{
	//if follower enemy, check distance to target, if its not good, then give velocity to target
	if (glm::distance(position, enemies[followObjectId]->position) > 2)
	{
		glm::vec3 force = glm::normalize(enemies[followObjectId]->position - position) * opponentSpeed;
		vel += force;
		usingTextures = true;
	}
	else
	{
		usingTextures = false;
	}
}
