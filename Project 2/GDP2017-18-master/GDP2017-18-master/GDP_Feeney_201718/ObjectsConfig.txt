Object Start
normals: true
physics: false
wireframe: false
texture1: chicken.bmp
texture1Strength: 1.0
filename: chicken.ply
position: 9.7,-20,8.5
orientation: 0,0,0
scale: 0.1
colour: 0.7,0.4,0.3,1.0
Object End

Object Start
normals: true
physics: false
wireframe: false
texture1: fox.bmp
texture1Strength: 1.0
filename: fox.ply
position: 9.7,0,800
orientation: 0,0,0
scale: 25
colour: 0.7,0.4,0.3,1.0
Object End

rightBoundary: 20
leftBoundary: -20
topBoundary: 20
bottomBoundary: -20
playerStartingPos: 0,0,0
playerSpeed: 5
numOpponents: 30
opponentSpeed: 0.3
cautiousStopDistance: 2.0
detectionDistance: 15.0
hitDistance: 1.0
playerHealth: 250
cameraOffset: 0,60,30

//more walls
Object Start
physics: false
wireframe: false
texture1: farmbackground.bmp
texture1Strength: 1.0
type: plane
filename: lava3.ply
position: 0,0,-20
orientation: 90,0,0
scale: 10
colour: 0.9,0.5,0.3,1.0
Object End

//more walls
Object Start
physics: false
wireframe: false
texture1: farmbackground.bmp
texture1Strength: 1.0
type: plane
filename: lava3.ply
position: 0,-30,20
orientation: 90,0,0
scale: 5
colour: 0.9,0.5,0.3,1.0
Object End

//more walls
Object Start
physics: false
wireframe: false
texture1: Ground.bmp
texture1Strength: 1.0
type: plane
filename: lava3.ply
position: -20,0,0
orientation: 0,0,-90
scale: 10
colour: 0.9,0.5,0.3,1.0
Object End

//more walls
Object Start
physics: false
wireframe: false
texture1: Ground.bmp
texture1Strength: 1.0
type: plane
filename: lava3.ply
position: 20,0,0
orientation: 0,0,90
scale: 10
colour: 0.9,0.5,0.3,1.0
Object End

//floor
Object Start
physics: false
wireframe: false
texture1: Ground.bmp
texture1Strength: 1.0
type: plane
filename: lava3.ply
position: 20,0,0
orientation: 0,0,0
scale: 10
colour: 0.9,0.5,0.3,1.0
Object End

Object Start
normals: true
physics: false
wireframe: false
skybox: true
filename: SmoothSphere_Inverted_Normals_xyz_n.ply
position: 0,0,0
orientation: 0,0,0
scale: 300
colour: 1,1,1,1.0
Object End


Light Start
colour_l: 1,1,1
position_l: 0.4,20.1,0
attentuation: 0,0.09,0
Light End

Light Start
colour_l: 1,1,1
position_l: 0.4,20.1,20
attentuation: 0,0.09,0
Light End

Light Start
colour_l: 1,1,1
position_l: 10.4,20.1,20
attentuation: 0,0.09,0
Light End

