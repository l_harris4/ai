#ifndef _cPlayer_HG_
#define _cPlayer_HG_
#include "cGameObject.h"

class cPlayer : public cGameObject
{
public:
	cPlayer(int healthIn);
	bool movingLeft = false;
	bool movingRight = false;
	bool movingUp = false;
	bool movingDown = false;
	int health;
	virtual void MakeDecisions();
};

#endif
