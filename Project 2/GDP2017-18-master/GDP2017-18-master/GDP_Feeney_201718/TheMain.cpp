// Include glad and GLFW in correct order
#include "globalOpenGL_GLFW.h"


#include <iostream>			// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr


#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>
#include <time.h>


#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"
#include "cModelAssetLoader.h"
#include <algorithm>
#include "cCamera.h"
#include "cNode.h"
#include <list>
#include <algorithm>
#include <gtest\gtest.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "pugixml.hpp"
#include "Physics.h"
#include "cLightManager.h"
#include "globalGameStuff.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include "mydll.h"
#include "cPlayer.h"
#include "cAngryEnemy.h"
#include "cCautiousEnemy.h"
#include "cFollowerEnemy.h"

void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID);
void PhysicsMainStep(double time);

extern void updateCamera();

FT_Library mft;

FT_Face mface;

//Data members need for AI project 2
std::vector< cGameObject* >  g_vecGameObjects;
std::vector< cGameObject* >  enemies;
float DETECTIONDISTANCE = 5.0f;
float HITDISTANCE = 1.0f;
float CAUTIOUSSTOPDISTANCE = 2.0f;
int playerHealth = 300;
glm::vec3 objectDestination;
int rightBoundary;
int leftBoundary;
int topBoundary;
int bottomBoundary;
glm::vec3 playerStartingPos;
float playerSpeed;
int numOpponents;
float opponentSpeed;
glm::vec3 cameraOffset;
cPlayer* thePlayer;


std::vector<std::string> loadedText;
int selectedLanguage = 6;

bool rightPathFromStart = true;
bool pathExists = true;
int pathCounter = 0;


cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr

//cShaderManager	g_ShaderManager;			// Stack (no new)
cShaderManager*		g_pShaderManager = 0;		// Heap, new (and delete)
cLightManager*		g_pLightManager = 0;
CTextureManager*		g_pTextureManager = 0;

cDebugRenderer*			g_pDebugRenderer = 0;
cCamera* g_pTheCamera = NULL;


// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
							// w = shininess of the 
GLint uniLoc_bIsDebugWireFrameObject = -1;
GLint uniLoc_bUsingLighting = -1;
GLint uniLoc_bUsingTextures = -1;
GLint uniLoc_bDiscardTexture = -1;

GLint uniLoc_eyePosition = -1;	// Camera position
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;

GLint texSampCube00_LocID = -1;
GLint texSampCube01_LocID = -1;
GLint texSampCube02_LocID = -1;
GLint texSampCube03_LocID = -1;

GLint texCubeBlend00_LocID = -1;
GLint texCubeBlend01_LocID = -1;
GLint texCubeBlend02_LocID = -1;
GLint texCubeBlend03_LocID = -1;

int g_selectedGameObjectIndex = 0;
int g_selectedLightIndex = 0;
bool g_movingGameObject = false;
bool g_lightsOn = false;
bool g_texturesOn = false;
bool g_movingLights = false;
bool g_boundingBoxes = false;
const float MOVESPEED = 0.1f;
const float ROTATIONSPEED = -2;
const float CAMERASPEED = 0.2f;








const char* vertexShaderText =
"#version 410\n"
"attribute vec4 coord;"
"varying vec2 texpos;"
"void main () {"
"	gl_Position = vec4(coord.xy, 0, 1);"
"	texpos = coord.zw;"
"}";

const char* fragmentShaderText =
"#version 410\n"
"varying vec2 texpos;"
"uniform bool igcolour;"
"uniform sampler2D tex;"
"uniform vec4 color;"
"void main () {"
" if(igcolour){"
"	gl_FragColor = vec4(texture2D(tex, texpos).xyz, 1);"
"}else{"
"	gl_FragColor = vec4(1, 1, 1, texture2D(tex, texpos).r) * color;"
"	}"
"}";

GLint attribute_coord;
GLint uniform_igcolour;
GLint uniform_tex;
GLint uniform_color;
GLuint mdp_vbo;
GLuint vertexShader;
GLuint fragmentShader;
GLuint textProgram;

//vertex array object
GLuint mvao;
GLuint mwidth = 0;
GLFWwindow* window;
GLuint mheight = 0;
//vertex buffer object
//cGameObject* cameraTargetObject = new cGameObject();


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

struct DistanceFunc
{
	DistanceFunc(const glm::vec3& _p) : p(_p) {}

	bool operator()(const cGameObject* lhs, const cGameObject* rhs) const
	{
		return glm::distance(p, lhs->position) > glm::distance(p, rhs->position);
	}

private:
	glm::vec3 p;
};

void loadText(std::string input_xml_file)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(input_xml_file.c_str());

	pugi::xml_node_iterator it = doc.children().begin();

	for (it; it != doc.children().end(); it++)
	{
		//we will find language children here
		pugi::xml_node_iterator it2 = it->children().begin();
		for (it2; it2 != it->children().end(); it2++)
		{
			loadedText.push_back(it2->child_value());
		}

	}


}

GLboolean initfreetype() {

	if (FT_Init_FreeType(&mft))
	{
		fprintf(stderr, "unable to init free type\n");
		return GL_FALSE;
	}

	if (FT_New_Face(mft, "assets/fonts/Digitalt.ttf", 0, &mface))
	{
		fprintf(stderr, "unable to open font\n");
		return GL_FALSE;
	}

	//set font size
	FT_Set_Pixel_Sizes(mface, 0, 48);


	if (FT_Load_Char(mface, 'X', FT_LOAD_RENDER))
	{
		fprintf(stderr, "unable to load character\n");
		return GL_FALSE;
	}


	return GL_TRUE;
}

GLboolean init_gl() {

	//create shaders
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderText, NULL);
	glCompileShader(vertexShader);

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderText, NULL);
	glCompileShader(fragmentShader);

	textProgram = glCreateProgram();
	glAttachShader(textProgram, vertexShader);
	glAttachShader(textProgram, fragmentShader);

	glLinkProgram(textProgram);

	//get vertex attribute/s id/s
	attribute_coord = glGetAttribLocation(textProgram, "coord");
	uniform_igcolour = glGetUniformLocation(textProgram, "igcolour");
	uniform_tex = glGetUniformLocation(textProgram, "tex");
	uniform_color = glGetUniformLocation(textProgram, "color");

	if (attribute_coord == -1 || uniform_tex == -1 || uniform_color == -1 || uniform_igcolour == -1) {
		fprintf(stderr, "unable to get attribute or uniform from shader\n");
		return GL_FALSE;
	}

	//generate and bind vbo 
	glGenBuffers(1, &mdp_vbo);

	//generate and bind vao
	glGenVertexArrays(1, &mvao);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDepthMask(GL_TRUE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	return GL_TRUE;
}

static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	mwidth = width;
	mheight = height;
	glViewport(0, 0, width, height);
}

GLboolean init_glfw() {

	//set error call back method
	glfwSetErrorCallback(error_callback);
	//init glfw
	if (!glfwInit()) {
		fprintf(stderr, "Unable to start glfw!");
		return GL_FALSE;

	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	//init window
	mwidth = 1024;
	mheight = 600;

	window = glfwCreateWindow(mwidth, mheight, "Media fundamentals...", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		fprintf(stderr, "Unable to create window!");
		return GL_FALSE;
	}
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetKeyCallback(window, key_callback);

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		fprintf(stderr, "Unable to init glad!");
		glfwTerminate();
		return GL_FALSE;

	}

	return GL_TRUE;
}


int main(int argc, char **argv)
{
	//init_glfw();
	initfreetype();

	loadText("assets/xml/intro.xml");
	srand(time(NULL));
	GLFWwindow* window;
	//    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
	{
		// exit(EXIT_FAILURE);
		std::cout << "ERROR: Couldn't init GLFW, so we're pretty much stuck; do you have OpenGL??" << std::endl;
		return -1;
	}

	

	int height = 700;	/* default */
	int width = 1200;	// default
	std::string title = "AI PROJECT 2";


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// General error string, used for a number of items during start up
	std::string error;

	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		return -1;
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;
	init_gl();

	::g_pDebugRenderer = new cDebugRenderer();
	if (!::g_pDebugRenderer->initialize(error))
	{
		std::cout << "Warning: couldn't init the debug renderer." << std::endl;
	}

	// Load models
	::g_pModelAssetLoader = new cModelAssetLoader();
	::g_pModelAssetLoader->setBasePath("assets/models/");


	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, ::g_pModelAssetLoader, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}
	//LoadModelsIntoScene();
	::g_pLightManager = new cLightManager();
	::g_pTheCamera = new cCamera();
	::g_pTheCamera->eye = glm::vec3(-10.0f, 20.0f, 10.0f);
	::g_pTheCamera->cameraMode = ::cCamera::eMode::FOLLOW_CAMERA;
	::g_pTheCamera->target = glm::vec3(0, 0, 0);
	::g_pTheCamera->theObject = new cGameObject();


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
	::g_pTextureManager = new CTextureManager();


	::g_pTextureManager->setBasePath("assets/textures");


	LoadModelsLightsFromFile();

	::g_pTextureManager->setBasePath("assets/textures/skybox");
	if (!::g_pTextureManager->CreateCubeTextureFromBMPFiles(
		"space",
		"SpaceBox_right1_posX.bmp",
		"SpaceBox_left2_negX.bmp",
		"SpaceBox_top3_posY.bmp",
		"SpaceBox_bottom4_negY.bmp",
		"SpaceBox_front5_posZ.bmp",
		"SpaceBox_back6_negZ.bmp", true, true))
	{
		std::cout << "Didn't load skybox" << std::endl;
	}



	::g_pLightManager->LoadShaderUniformLocations(currentProgID);

	//creating all the objects needed for AI project 2
	//create the player
	cPlayer* pPlayerGO = new cPlayer(playerHealth);
	pPlayerGO->bIsUpdatedInPhysics = true;
	pPlayerGO->typeOfObject = eTypeOfObject::PLAYER;
	pPlayerGO->radius = 1.0f;
	sMeshDrawInfo meshInfo;
	pPlayerGO->vecMeshes.push_back(meshInfo);
	meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	::g_vecGameObjects.push_back(pPlayerGO);
	::g_vecGameObjects.back()->vecMeshes = ::g_vecGameObjects[0]->vecMeshes;
	::g_vecGameObjects.back()->scale = ::g_vecGameObjects[0]->scale;
	pPlayerGO->position = playerStartingPos;

	thePlayer = (cPlayer*)::g_vecGameObjects.back();

	//create the opponents
	for (int i = 0; i < numOpponents; ++i)
	{
		int randType = rand() % 3;
		randType += 5;
		cGameObject* pTempGO;
		if (randType == eTypeOfObject::ANGRYENEMY)
		{
			pTempGO = new cAngryEnemy();
		}
		else if (randType == eTypeOfObject::CAUTIOUSENEMY)
		{
			pTempGO = new cCautiousEnemy();
		}
		else if (randType == eTypeOfObject::FOLLOWERENEMY)
		{
			pTempGO = new cFollowerEnemy();
		}


		
		pTempGO->bIsUpdatedInPhysics = true;
		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
		pTempGO->radius = 1.0f;
		sMeshDrawInfo meshInfo;
		pTempGO->vecMeshes.push_back(meshInfo);
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		::g_vecGameObjects.push_back(pTempGO);
	
		//calculate a random position for the new enemy
		::g_vecGameObjects.back()->position.y = 0;
		::g_vecGameObjects.back()->position.x = rand() % 40 - 20;
		::g_vecGameObjects.back()->position.z = rand() % 40 - 20;

		//also randomize enemy type	
		::g_vecGameObjects.back()->typeOfObject = (eTypeOfObject)randType;

		if (randType == eTypeOfObject::ANGRYENEMY)
		{
			::g_vecGameObjects.back()->vecMeshes = ::g_vecGameObjects[1]->vecMeshes;
			::g_vecGameObjects.back()->scale = ::g_vecGameObjects[1]->scale;
		}
		else if (randType == eTypeOfObject::CAUTIOUSENEMY)
		{
			::g_vecGameObjects.back()->vecMeshes = ::g_vecGameObjects[0]->vecMeshes;
			::g_vecGameObjects.back()->scale = ::g_vecGameObjects[0]->scale;
		}
		else if (randType == eTypeOfObject::FOLLOWERENEMY)
		{
			::g_vecGameObjects.back()->vecMeshes = ::g_vecGameObjects[0]->vecMeshes;
			::g_vecGameObjects.back()->scale = ::g_vecGameObjects[0]->scale * 0.5f;
			::g_vecGameObjects.back()->diffuseColour = glm::vec4(1,1,0,1);
		}

		enemies.push_back(::g_vecGameObjects.back());
	}

	//set the walls in proper position
	::g_vecGameObjects[2]->position.z = bottomBoundary;
	::g_vecGameObjects[3]->position.z = topBoundary +2;
	::g_vecGameObjects[3]->vecMeshes[0].bDisableBackFaceCulling = true;
	::g_vecGameObjects[4]->position.x = leftBoundary;
	::g_vecGameObjects[5]->position.x = rightBoundary;

	//for all the followers find something to follow
	for (int i = 0; i < enemies.size(); ++i)
	{
		if (enemies[i]->typeOfObject == eTypeOfObject::FOLLOWERENEMY)
		{
			int randID = rand() % enemies.size();
			while (randID == i)
			{
				randID = rand() % enemies.size();
			}

			enemies[i]->followObjectId = randID;
		}
	}


glEnable(GL_DEPTH);

// Gets the "current" time "tick" or "step"
double lastTimeStep = glfwGetTime();


GLfloat red[4] = { 1, 0, 0, 1 };

// Main game or application loop
while (!glfwWindowShouldClose(window))
{

	double curTime = glfwGetTime();
	double deltaTime = curTime - lastTimeStep;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	RenderScene(::g_vecGameObjects, window, deltaTime);
	char buffer[33];

	//Set the window title the current health
	std::string healthString = itoa(thePlayer->health, buffer, 10);
	healthString = "Health: " + healthString;

	glfwSetWindowTitle(window, healthString.c_str());
	glfwSwapBuffers(window);
	glfwPollEvents();

	lastTimeStep = curTime;

	//The following is basically a physics step
	PhysicsMainStep(deltaTime);

	::g_pTheCamera->target = thePlayer->position;
	updateCamera();

}// while ( ! glfwWindowShouldClose(window) )


glfwDestroyWindow(window);
glfwTerminate();

// 
delete ::g_pShaderManager;
delete ::g_pVAOManager;

return 0;
}

float OrientAngle(glm::vec2 a) {
	float angle = atan2(a.x, a.y);
	return angle;
}

void PhysicsMainStep(double time)
{
	int size = g_vecGameObjects.size();
	for (int i = 0; i < size; ++i)
	{
		if (g_vecGameObjects[i]->bIsUpdatedInPhysics)
		{
			//update the velocity based on acceleration
			g_vecGameObjects[i]->vel += g_vecGameObjects[i]->accel * (float)time;
			//update the position based on velocity
			g_vecGameObjects[i]->position += g_vecGameObjects[i]->vel * (float)time;

			//if the object is outside the boundaries move it back inside
			if (g_vecGameObjects[i]->position.x > rightBoundary)
			{
				g_vecGameObjects[i]->position.x = rightBoundary;
			}

			if (g_vecGameObjects[i]->position.x < leftBoundary)
			{
				g_vecGameObjects[i]->position.x = leftBoundary;
			}

			if (g_vecGameObjects[i]->position.z > topBoundary)
			{
				g_vecGameObjects[i]->position.z = topBoundary;
			}

			if (g_vecGameObjects[i]->position.z < bottomBoundary)
			{
				g_vecGameObjects[i]->position.z = bottomBoundary;
			}
		}

		g_vecGameObjects[i]->MakeDecisions();

		if (g_vecGameObjects[i]->vel.x != 0 || g_vecGameObjects[i]->vel.z != 0)
		{
			float angle = OrientAngle(glm::vec2(g_vecGameObjects[i]->vel.x, g_vecGameObjects[i]->vel.z));
			glm::vec3 angleToBe = glm::vec3(0, glm::radians(glm::degrees(angle) - 0.0f), 0);
			g_vecGameObjects[i]->overwriteQOrientationFormEuler(angleToBe);
		}// end if

		g_vecGameObjects[i]->vel = g_vecGameObjects[i]->vel *0.9f;

		if (glm::length(g_vecGameObjects[i]->vel) < 0.03)
		{
			g_vecGameObjects[i]->vel = glm::vec3(0);
		}
	
	}
}

void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID)
{
	switch (samplerIndex)
	{
	case 0:
		glUniform1i(texSampCube00_LocID, textureUnitID);
		glUniform1f(texCubeBlend00_LocID, blendRatio);
		break;
	case 1:
		glUniform1i(texSampCube01_LocID, textureUnitID);
		glUniform1f(texCubeBlend01_LocID, blendRatio);
		break;
	case 2:
		glUniform1i(texSampCube02_LocID, textureUnitID);
		glUniform1f(texCubeBlend02_LocID, blendRatio);
		break;
	case 3:
		glUniform1i(texSampCube03_LocID, textureUnitID);
		glUniform1f(texCubeBlend03_LocID, blendRatio);
		break;
	default:
		// Invalid samplerIndex;
		break;
	}//switch (samplerIndex)
	return;
}//void setCubeSamplerAndBlenderByIndex()

