--------------------------------------------
CONTROLS
--------------------------------------------
Arrow keys to control the camera
Up and down to zoom in and out
Left and right to pan



NOTE: 
the files relavent for the project are cNode.h, cNode.cpp, and theMain.cpp


With default settings the project will show the determining of the path first, then when it reaches the end
the correct found path will be taken back to the start. If you would rather the full path be calculated before
the flag starts to move then simply change the rightPathFromStart variable near the top to true.

