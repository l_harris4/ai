#ifndef _cCounterGuide_HG_
#define _cCounterGuide_HG_
#include "cGameObject.h"
#include "cNode.h"

enum CounterGuideState
{
	WANDERING = 0,
	CHASING = 1,
	DEATH = 2
};

class cCounterGuide : public cGameObject
{
public:
	cCounterGuide();
	virtual void MakeDecisions();
	CounterGuideState state;
	bool moving = true;
	glm::vec3 destination;
	cGameObject* theTarget;
};

#endif


