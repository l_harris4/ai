// Include glad and GLFW in correct order
#include "globalOpenGL_GLFW.h"


#include <iostream>			// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr


#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>
#include <time.h>

#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"
#include "cModelAssetLoader.h"
#include <algorithm>
#include "cCamera.h"
#include "cNode.h"
#include "cMazeMaker.h"
#include <list>
#include <algorithm>
#include "cGuide.h"
#include "cCounterGuide.h"
#include "cMinotaur.h"


#include "Physics.h"	// Physics collision detection functions

#include "cLightManager.h"

// Include all the things that are accessed in other files
#include "globalGameStuff.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

//void DrawParticle(cParticle* pThePart);
void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID);

extern void updateCamera();

std::vector< cGameObject* >  g_vecGameObjects;
std::vector< cGameObject* >  g_vecGuides;
cMazeMaker* mazeManager = 0;
int mazeSize = 15;



//ALL of the following is needed for pathfinding/////////////////////////////////////////////////////////////////////////
//this variable determines if the right path is found before moving
bool rightPathFromStart = true;
//a variable to keep track of all the nodes
std::vector<cNode> allNodes;
//a location of where the starting node is
glm::vec2 nodeStart;
//a location of where the ending node is
glm::vec2 nodeEnd;
//a pointer to the starting node
cNode* startingNode;
//a pointer to the ending node
cNode* endingNode;
//a pointer to the current node being considered
cNode* currentNode;
//this distance determines how close a node has to be to another in order to be considered connected
const float ACCEPTABLEDISTANCE = 2.5f;
//a variable to keep track of if the flag is in transit to another node
bool mainObjectMoving = false;
//a variable to keep track of if the path has been found yet or not
bool findingPath = true;
//the destination for the object to be currently moving towards
glm::vec3 objectDestination;
//to variable to keep track of if a path to the destination actually exists
bool pathExists = true;
//path counter is used when determining the next node when the path is already determined
int pathCounter = 0;
//a vector to keep track of only nodes on the correct path
std::vector<cNode*> rightPathNodes;
std::vector<cNode*>* goodNodes;

glm::vec2 entrancePosition = glm::vec2(-1, -1);
glm::vec2 exitPosition = glm::vec2(-1, -1);

//list of nodes to be considered in pathfinding
std::list<cNode*> openNodes;
//list of nodes already considered in pathfinding
std::list<cNode*> closedNodes;

float playerHealth = 300;

std::map<std::pair<std::pair<int, int>, std::pair<int, int>>, std::vector<cNode*>>* allPaths;
//std::map< std::pair<int, int>, std::map<std::pair<int, int>, std::vector<cNode*>>> allPaths;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr
cShaderManager*		g_pShaderManager = 0;		// Heap, new (and delete)
cLightManager*		g_pLightManager = 0;
CTextureManager*		g_pTextureManager = 0;

cDebugRenderer*			g_pDebugRenderer = 0;
cCamera* g_pTheCamera = NULL;


// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1;
GLint uniLoc_materialSpecular = -1;
GLint uniLoc_bIsDebugWireFrameObject = -1;
GLint uniLoc_bUsingLighting = -1;
GLint uniLoc_bUsingTextures = -1;
GLint uniLoc_bDiscardTexture = -1;
GLint uniLoc_eyePosition = -1;
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;
GLint texSampCube00_LocID = -1;
GLint texSampCube01_LocID = -1;
GLint texSampCube02_LocID = -1;
GLint texSampCube03_LocID = -1;
GLint texCubeBlend00_LocID = -1;
GLint texCubeBlend01_LocID = -1;
GLint texCubeBlend02_LocID = -1;
GLint texCubeBlend03_LocID = -1;

int g_selectedGameObjectIndex = 0;
int g_selectedLightIndex = 0;
bool g_movingGameObject = false;
bool g_lightsOn = false;
bool g_texturesOn = false;
bool g_movingLights = false;
bool g_boundingBoxes = false;
const float MOVESPEED = 0.1f;
const float ROTATIONSPEED = -2;
const float CAMERASPEED = 0.2f;


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

void populatePathVector(int startX, int startZ, int endX, int endZ)
{
	//clear all the old vectors
	openNodes.clear();
	closedNodes.clear();
	rightPathNodes.clear();
	//find the starting node and ending node
	int allNodesSize = allNodes.size();
	int foundCount = 0;
	cNode* startingNodeV;
	cNode* currentNodeV;
	cNode* endingNodeV;
	for (int i = 0; i < allNodesSize; ++i)
	{
		if (allNodes[i].xLoc == startX && allNodes[i].zLoc == startZ)
		{
			startingNodeV = &allNodes[i];
			foundCount++;
		}
		if (allNodes[i].xLoc == endX && allNodes[i].zLoc == endZ)
		{
			endingNodeV = &allNodes[i];
			foundCount++;
		}
		if (foundCount == 2)
			break;
	}

	currentNodeV = startingNodeV;

	while (currentNodeV != endingNodeV)
	{
		//this is where the pathfinding logic will take place
		closedNodes.push_back(currentNodeV);
		//take self out of open nodes
		openNodes.remove(currentNodeV);


		for (int i = 0; i < currentNodeV->connectedNodes.size(); ++i)
		{
			//this is so we dont reprocess a node
			if (currentNodeV->connectedNodes[i]->fValue == 0)
			{
				//process the node
				currentNodeV->connectedNodes[i]->gValue = currentNodeV->gValue + 1;
				currentNodeV->connectedNodes[i]->fValue = currentNodeV->connectedNodes[i]->gValue + currentNodeV->connectedNodes[i]->hValue;
				openNodes.push_back(currentNodeV->connectedNodes[i]);
			}
		}

		if (openNodes.empty())
		{
			pathExists = false;
			break;
		}
		cNode* nextNode = openNodes.back();
		int lowestF = nextNode->fValue;
		//now go through the open nodes and find the one with least f value 
		for (std::list<cNode*>::iterator iterator = openNodes.begin(), end = openNodes.end(); iterator != end; ++iterator) {
			if ((*iterator)->fValue < lowestF)
			{
				lowestF = (*iterator)->fValue;
				nextNode = (*iterator);
			}
			if ((*iterator) == endingNodeV)
			{
				nextNode = endingNodeV;
				break;
			}
		}
		//set that to current node
		currentNodeV = nextNode;

	}

	rightPathNodes.push_back(currentNodeV);
	//first run through current node should be the end node
	while (currentNodeV != startingNodeV)
	{
		std::vector<cNode*> backtrackNodes;
		//to pick the next node we have to pick a node that is adjacent and in the list of closed nodes
		for (int i = 0; i < currentNodeV->connectedNodes.size(); ++i)
		{
			if (currentNodeV->connectedNodes[i] == startingNodeV)
			{
				backtrackNodes.clear();
				backtrackNodes.push_back(currentNodeV->connectedNodes[i]);
				break;
			}
			if (std::find(closedNodes.begin(), closedNodes.end(), currentNodeV->connectedNodes[i]) != closedNodes.end())
			{
				backtrackNodes.push_back(currentNodeV->connectedNodes[i]);
			}
		}
		//if there are multiple then have to select the one with the lowest g value
		if (backtrackNodes.size() > 1)
		{
			cNode* selected = backtrackNodes[0];
			for (int i = 1; i < backtrackNodes.size(); ++i)
			{
				if (backtrackNodes[i]->gValue < selected->gValue)
					selected = backtrackNodes[i];
			}
			currentNodeV = selected;
		}
		else
		{
			currentNodeV = backtrackNodes[0];
		}

		rightPathNodes.push_back(currentNodeV);
	}

	//push the vector into 
	(*allPaths)[std::make_pair(std::make_pair(startX, startZ), std::make_pair(endX, endZ))] = rightPathNodes;

	//go through all nodes and set their f values to zero again
	for (int i = 0; i < allNodesSize; ++i)
	{
		allNodes[i].fValue = 0;
	}

}


int main(void)
{

	srand(time(NULL));
	GLFWwindow* window;
	//    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
	{
		// exit(EXIT_FAILURE);
		std::cout << "ERROR: Couldn't init GLFW, so we're pretty much stuck; do you have OpenGL??" << std::endl;
		return -1;
	}

	int height = 700;	/* default */
	int width = 1200;	// default
	std::string title = "AI PROJECT 3";


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// C++ string
	// C no strings. Sorry. char    char name[7] = "Michael\0";
	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// General error string, used for a number of items during start up
	std::string error;

	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	::g_pDebugRenderer = new cDebugRenderer();
	if (!::g_pDebugRenderer->initialize(error))
	{
		std::cout << "Warning: couldn't init the debug renderer." << std::endl;
	}

	// Load models
	::g_pModelAssetLoader = new cModelAssetLoader();
	::g_pModelAssetLoader->setBasePath("assets/models/");


	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, ::g_pModelAssetLoader, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}
	//LoadModelsIntoScene();
	::g_pLightManager = new cLightManager();
	::g_pTheCamera = new cCamera();
	::g_pTheCamera->eye = glm::vec3(-10.0f, 20.0f, 10.0f);
	::g_pTheCamera->cameraMode = ::cCamera::eMode::FOLLOW_CAMERA;
	::g_pTheCamera->target = glm::vec3(0, 0, 0);
	::g_pTheCamera->theObject = new cGameObject();


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
	::g_pTextureManager = new CTextureManager();


	::g_pTextureManager->setBasePath("assets/textures");


	LoadModelsLightsFromFile();

	::g_pTextureManager->setBasePath("assets/textures/skybox");
	if (!::g_pTextureManager->CreateCubeTextureFromBMPFiles(
		"space",
		"SpaceBox_right1_posX.bmp",
		"SpaceBox_left2_negX.bmp",
		"SpaceBox_top3_posY.bmp",
		"SpaceBox_bottom4_negY.bmp",
		"SpaceBox_front5_posZ.bmp",
		"SpaceBox_back6_negZ.bmp", true, true))
	{
		std::cout << "Didn't load skybox" << std::endl;
	}

	allPaths = new std::map<std::pair<std::pair<int, int>, std::pair<int, int>>, std::vector<cNode*>>();

	::g_pLightManager->LoadShaderUniformLocations(currentProgID);

	mazeManager = new cMazeMaker();
	mazeManager->GenerateMaze(mazeSize, mazeSize);

	int mazeColumnSize = mazeManager->maze.size();
	for (int i = 0; i < mazeColumnSize; ++i)
	{
		int mazeRowSize = mazeManager->maze[i].size();
		for (int j = 0; j < mazeRowSize; ++j)
		{
			cNode node;
			node.xLoc = i * 2;
			node.zLoc = j * 2;
			//2 and 3 are the objects we want
			if (mazeManager->maze[i][j][0] == true)
				node.isValid = false;
			else
				node.isValid = true;

			allNodes.push_back(node);
		}
	}

	int foundIndex = 0;
	//finding the entrance and exit to the maze
	int testingRowColumn = 0;
	for (int i = 0; i < mazeColumnSize; ++i)
	{
		if (mazeManager->maze[testingRowColumn][i][0] != true)
		{
			if (foundIndex == 0)
			{
				entrancePosition = glm::vec2(testingRowColumn, i);
				foundIndex++;
			}
			else if (foundIndex == 1)
			{
				exitPosition = glm::vec2(testingRowColumn, i);
				foundIndex++;
			}
			else
				break;
		}
	}

	testingRowColumn = mazeColumnSize - 1;
	for (int i = 0; i < mazeColumnSize; ++i)
	{
		if (mazeManager->maze[testingRowColumn][i][0] != true)
		{
			if (foundIndex == 0)
			{
				entrancePosition = glm::vec2(testingRowColumn, i);
				foundIndex++;
			}
			else if (foundIndex == 1)
			{
				exitPosition = glm::vec2(testingRowColumn, i);
				foundIndex++;
			}
			else
				break;
		}
	}

	testingRowColumn = 0;
	for (int i = 0; i < mazeColumnSize; ++i)
	{
		if (mazeManager->maze[i][testingRowColumn][0] != true)
		{
			if (foundIndex == 0)
			{
				entrancePosition = glm::vec2(i, testingRowColumn);
				foundIndex++;
			}
			else if (foundIndex == 1)
			{
				exitPosition = glm::vec2(i, testingRowColumn);
				foundIndex++;
			}
			else
				break;
		}
	}

	testingRowColumn = mazeColumnSize - 1;
	for (int i = 0; i < mazeColumnSize; ++i)
	{
		if (mazeManager->maze[i][testingRowColumn][0] != true)
		{
			if (foundIndex == 0)
			{
				entrancePosition = glm::vec2(i, testingRowColumn);
				foundIndex++;
			}
			else if (foundIndex == 1)
			{
				exitPosition = glm::vec2(i, testingRowColumn);
				foundIndex++;
			}
			else
				break;
		}
	}
	nodeStart = entrancePosition * 2.0f;
	nodeEnd = exitPosition * 2.0f;

	//connect adjacent nodes
	//go through all nodes and if another node is close enough, identify it as a connected node
	int numNodes = allNodes.size();
	for (int i = 0; i < numNodes; i++)
	{
		glm::vec2 tempPos;
		tempPos.x = allNodes[i].xLoc;
		tempPos.y = allNodes[i].zLoc;

		if (tempPos == nodeStart)
			startingNode = &allNodes[i];

		if (tempPos == nodeEnd)
			endingNode = &allNodes[i];

		//also calculate the h value for the node
		allNodes[i].hValue += abs(nodeEnd.x - tempPos.x) / 2;
		allNodes[i].hValue += abs(nodeEnd.y - tempPos.y) / 2;

		for (int j = 0; j < numNodes; j++)
		{
			glm::vec2 firstPos;
			firstPos.x = allNodes[j].xLoc;
			firstPos.y = allNodes[j].zLoc;
			//if the other node is pretty close, add it to connected nodes
			if (i != j && allNodes[j].isValid)
			{
				glm::vec2 secondPos;
				secondPos.x = allNodes[i].xLoc;
				secondPos.y = allNodes[i].zLoc;
				if (glm::distance(firstPos, secondPos) < ACCEPTABLEDISTANCE)
				{
					allNodes[i].connectedNodes.push_back(&allNodes[j]);
				}
			}
		}
	}


	//Now place the flag objects for current positon, starting position, and ending position
	g_vecGameObjects[1]->position.x = startingNode->xLoc;
	g_vecGameObjects[1]->position.z = startingNode->zLoc;
	g_vecGameObjects[1]->position.y = 2;

	g_vecGameObjects[2]->position.x = endingNode->xLoc;
	g_vecGameObjects[2]->position.z = endingNode->zLoc;
	g_vecGameObjects[2]->position.y = 2;

	g_vecGameObjects[3]->position.x = startingNode->xLoc;
	g_vecGameObjects[3]->position.z = startingNode->zLoc;
	g_vecGameObjects[3]->position.y = 2;

	objectDestination.x = endingNode->xLoc;
	objectDestination.z = endingNode->zLoc;
	objectDestination.y = 2;

	currentNode = startingNode;

	//go through all nodes making a vector of all good nodes
	goodNodes = new std::vector<cNode*>();
	int allNodesSize = allNodes.size();
	for (int nodeIndex = 0; nodeIndex < allNodesSize; nodeIndex++)
	{
		if (allNodes[nodeIndex].isValid)
			goodNodes->push_back(&allNodes[nodeIndex]);
	}

	//go through all the good nodes populating paths
	int goodNodesSize = goodNodes->size();
	for (int nodeIndex = 0; nodeIndex < goodNodesSize; nodeIndex++)
	{
		for (int nodeIndexInner = 0; nodeIndexInner < goodNodesSize; nodeIndexInner++)
		{
			if (nodeIndex != nodeIndexInner)
			{
				populatePathVector((*goodNodes)[nodeIndex]->xLoc, (*goodNodes)[nodeIndex]->zLoc,
					(*goodNodes)[nodeIndexInner]->xLoc, (*goodNodes)[nodeIndexInner]->zLoc);
			}
		}
	}

	openNodes.clear();
	closedNodes.clear();
	rightPathNodes.clear();

	//if we only want to see the calculate path, then have to calculate it before rendering

	rightPathNodes = (*allPaths)[std::make_pair(std::make_pair(nodeStart.x, nodeStart.y), std::make_pair(nodeEnd.x, nodeEnd.y))];
	currentNode = rightPathNodes[rightPathNodes.size() - 2];
	pathCounter = rightPathNodes.size() - 2;

	glEnable(GL_DEPTH);
	//create the opponents
	{
		cGameObject* pGuide;
		pGuide = new cGuide();

		pGuide->bIsUpdatedInPhysics = true;
		pGuide->typeOfObject = eTypeOfObject::GUIDE;
		pGuide->radius = 1.0f;
		sMeshDrawInfo meshInfo;
		pGuide->qOrientation = ::g_vecGameObjects[1]->qOrientation;
		pGuide->vecMeshes.push_back(meshInfo);
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		::g_vecGameObjects.push_back(pGuide);
		::g_vecGuides.push_back(pGuide);

		//calculate a random position for the new enemy
		::g_vecGameObjects.back()->position.y = 4;
		::g_vecGameObjects.back()->position.x = rand() % 40 - 20;
		::g_vecGameObjects.back()->position.z = rand() % 40 - 20;

		::g_vecGameObjects.back()->vecMeshes = ::g_vecGameObjects[1]->vecMeshes;
		::g_vecGameObjects.back()->scale = ::g_vecGameObjects[1]->scale;
	}

	{
		cGameObject* pCounterGuide;
		pCounterGuide = new cCounterGuide();

		pCounterGuide->bIsUpdatedInPhysics = true;
		pCounterGuide->typeOfObject = eTypeOfObject::COUNTERGUIDE;
		pCounterGuide->radius = 1.0f;
		sMeshDrawInfo meshInfo;
		pCounterGuide->qOrientation = ::g_vecGameObjects[2]->qOrientation;
		pCounterGuide->vecMeshes.push_back(meshInfo);
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		::g_vecGameObjects.push_back(pCounterGuide);

		//calculate a random position for the new enemy
		::g_vecGameObjects.back()->position.y = 4;
		::g_vecGameObjects.back()->position.x = rand() % 40 - 20;
		::g_vecGameObjects.back()->position.z = rand() % 40 - 20;

		::g_vecGameObjects.back()->vecMeshes = ::g_vecGameObjects[2]->vecMeshes;
		::g_vecGameObjects.back()->scale = ::g_vecGameObjects[2]->scale;
	}

	{
		cGameObject* pCounterGuide;
		pCounterGuide = new cMinotaur();

		pCounterGuide->bIsUpdatedInPhysics = true;
		pCounterGuide->typeOfObject = eTypeOfObject::MINOTAUR;
		pCounterGuide->radius = 1.0f;
		sMeshDrawInfo meshInfo;
		pCounterGuide->qOrientation = ::g_vecGameObjects[3]->qOrientation;
		pCounterGuide->vecMeshes.push_back(meshInfo);
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		::g_vecGameObjects.push_back(pCounterGuide);

		::g_vecGameObjects.back()->position = ::g_vecGameObjects[1]->position;
		::g_vecGameObjects.back()->vecMeshes = ::g_vecGameObjects[3]->vecMeshes;
		::g_vecGameObjects.back()->scale = ::g_vecGameObjects[3]->scale;
	}

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();
	::g_pTheCamera->theObject = ::g_vecGameObjects[1];
	::g_pTheCamera->target = ::g_vecGameObjects[1]->position;


	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		//sortObjectsBasedOnCamera();
		// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		::g_pTheCamera->updateTick(deltaTime);

		RenderScene(::g_vecGameObjects, window, deltaTime);


		//Set the window title the current health
		std::string healthString = "Health: " + std::to_string((int)playerHealth);

		glfwSetWindowTitle(window, healthString.c_str());
		// "Presents" what we've drawn
		// Done once per scene 
		glfwSwapBuffers(window);
		glfwPollEvents();

		int allGameObjectsSize = ::g_vecGameObjects.size();
		for (int i = 0; i < allGameObjectsSize; ++i)
		{
			g_vecGameObjects[i]->MakeDecisions();
			if (::g_vecGameObjects[i]->bIsUpdatedInPhysics)
			{
				::g_vecGameObjects[i]->position += ::g_vecGameObjects[i]->vel * (float)deltaTime * 5.0f;
			}
		}

		//if the main object is moving, move it closer to its destination
		if (mainObjectMoving)
		{
			if (glm::distance(::g_vecGameObjects[1]->position, objectDestination) <= 0.1)
			{
				mainObjectMoving = false;
				::g_vecGameObjects[1]->position = objectDestination;
				::g_vecGameObjects[1]->vel = glm::vec3(0, 0, 0);
			}
			::g_pTheCamera->theObject = ::g_vecGameObjects[1];
			::g_pTheCamera->target = ::g_vecGameObjects[1]->position;
		}

		lastTimeStep = curTime;

	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;
	delete ::mazeManager;

	//    exit(EXIT_SUCCESS);
	return 0;
}

void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID)
{
	switch (samplerIndex)
	{
	case 0:
		glUniform1i(texSampCube00_LocID, textureUnitID);
		glUniform1f(texCubeBlend00_LocID, blendRatio);
		break;
	case 1:
		glUniform1i(texSampCube01_LocID, textureUnitID);
		glUniform1f(texCubeBlend01_LocID, blendRatio);
		break;
	case 2:
		glUniform1i(texSampCube02_LocID, textureUnitID);
		glUniform1f(texCubeBlend02_LocID, blendRatio);
		break;
	case 3:
		glUniform1i(texSampCube03_LocID, textureUnitID);
		glUniform1f(texCubeBlend03_LocID, blendRatio);
		break;
	default:
		// Invalid samplerIndex;
		break;
	}//switch (samplerIndex)
	return;
}//void setCubeSamplerAndBlenderByIndex()


//
// //if taking the correct path from start and that path actualy exists
//if (rightPathFromStart && pathExists)
//{
//	//set the object destination
//	objectDestination.x = currentNode->xLoc;
//	objectDestination.z = currentNode->zLoc;
//	objectDestination.y = 2;
//	//if the main object is moving, move it closer to its destination
//	if (mainObjectMoving)
//	{
//		::g_vecGameObjects[1]->position += glm::normalize(objectDestination - ::g_vecGameObjects[1]->position) * MOVESPEED;
//		if (glm::distance(::g_vecGameObjects[1]->position, objectDestination) <= MOVESPEED)
//		{
//			mainObjectMoving = false;
//			::g_vecGameObjects[1]->position = objectDestination;
//		}
//		::g_pTheCamera->theObject = ::g_vecGameObjects[1];
//		::g_pTheCamera->target = ::g_vecGameObjects[1]->position;
//	}
//	else if (currentNode != endingNode) //if the object hasnt moved all the way to the end, set the next node to be the current node
//	{
//		pathCounter--;
//		currentNode = rightPathNodes[pathCounter];
//		mainObjectMoving = true;
//	}
//
//}

