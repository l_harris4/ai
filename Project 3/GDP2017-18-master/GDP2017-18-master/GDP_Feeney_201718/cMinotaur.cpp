#include "cMinotaur.h"
#include "globalOpenGL_GLFW.h"
#include <map>
#include <iostream>
extern std::vector< cGameObject* >  g_vecGameObjects;
extern std::vector<cNode*>* goodNodes;
extern std::map<std::pair<std::pair<int, int>, std::pair<int, int>>, std::vector<cNode*>>* allPaths;
extern float playerHealth;

cMinotaur::cMinotaur()
{
	lastTimeStep = glfwGetTime();
	waitTimeReset = 4.0f;
	waitTime = waitTimeReset;
	state = MinotaurGuideState::IDLE;
}

void cMinotaur::MakeDecisions()
{
	double curTime = glfwGetTime();
	double deltaTime = curTime - lastTimeStep;
	switch (state)
	{
	case MinotaurGuideState::IDLE:
	{
		this->vel = glm::vec3(0, 0, 0);
		waitTime -= deltaTime;
		//if done waiting change the state and reset the time
		if (waitTime < 0)
		{
			waitTime = waitTimeReset;

			//find the path for the minotaur to get from where it is to the player
			//find the current node
			int goodNodesSize = goodNodes->size();
			int xLoc = ((int)g_vecGameObjects[1]->position.x / 2) * 2;
			int zLoc = ((int)g_vecGameObjects[1]->position.z / 2) * 2;

			int xLoc2 = std::ceil((this->position.x / 2.0f) * 2.0f);
			int zLoc2 = std::ceil((this->position.z / 2.0f) * 2.0f);
			float distanceToNode = 999;

			int foundNodeNum = 0;
			for (int i = 0; i < goodNodesSize; ++i)
			{
				if ((*goodNodes)[i]->xLoc == xLoc && (*goodNodes)[i]->zLoc == zLoc)
				{
					foundNodeNum++;
					endingNode = (*goodNodes)[i];
				}

				float tempDist = glm::distance(glm::vec2((*goodNodes)[i]->xLoc, (*goodNodes)[i]->zLoc), glm::vec2(xLoc2, zLoc2));
				if (tempDist < distanceToNode)
				{
					distanceToNode = tempDist;
					currentNode = (*goodNodes)[i];
				}

			}

			if (foundNodeNum == 1 && currentNode != endingNode)
			{
				//set the path counter
				//set the vector of paths
				rightPathNodes = (*allPaths)[std::make_pair(std::make_pair(currentNode->xLoc, currentNode->zLoc), std::make_pair(endingNode->xLoc, endingNode->zLoc))];
				currentNode = rightPathNodes[rightPathNodes.size() - 2];
				pathCounter = rightPathNodes.size() - 2;
				//then change the state to demonstrating and change the path
				this->state = MinotaurGuideState::CHASINGPLAYER;
			}
		}
	}
	break;
	case MinotaurGuideState::CHASINGPLAYER:
	{
		//set the object destination
		objectDestination.x = currentNode->xLoc;
		objectDestination.z = currentNode->zLoc;
		objectDestination.y = 2;
		//if the main object is moving, move it closer to its destination
		if (moving)
		{
			this->vel = glm::normalize(objectDestination - this->position) * 0.5f;
			if (glm::distance(this->position, objectDestination) <= 0.1f)
			{
				moving = false;
				this->position = objectDestination;
			}
		}
		else if (currentNode != endingNode) //if the object hasnt moved all the way to the end, set the next node to be the current node
		{
			pathCounter--;
			currentNode = rightPathNodes[pathCounter];
			moving = true;
		}
		else //this will trigger when the minotaur has made it to its destination
		{
			//set state to waiting again
			this->state = MinotaurGuideState::IDLE;
		}

		//test the distance from the minotaur to the player, if it is pretty close then change state to hurting player
		if (glm::distance(g_vecGameObjects[1]->position, this->position) < 1)
		{
			state = MinotaurGuideState::HURTINGPLAYER;
		}
	}
	break;
	case MinotaurGuideState::HURTINGPLAYER:
	{
		this->vel = glm::vec3(0, 0, 0);
		//reduce the players health
		playerHealth -= 0.1;
		// test the distance, if it becomes too much then change state to waiting
		float distance = glm::distance(g_vecGameObjects[1]->position, this->position);
		if (distance > 3)
		{
			state = MinotaurGuideState::IDLE;
		}
		
	}
	break;
	}

	lastTimeStep = curTime;
}
