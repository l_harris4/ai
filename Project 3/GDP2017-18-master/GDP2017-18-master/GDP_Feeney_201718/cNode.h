#pragma once
#ifndef _CNODE_HG_
#define _CNODE_HG_


#include <vector>
#include <string>

class cNode
{
public:
	//ctor
	cNode();
	//ctor with id
	cNode(std::string id);
	//dtor
	~cNode();

	//data members
	std::string Id;
	std::vector<cNode*> connectedNodes;
	int xLoc;
	int zLoc;
	int lowestJumpNum;
	std::string shortestPath;
	bool isValid;
	int gValue;
	int hValue;
	int fValue;

	//methods
	void AddConnection(cNode& nodeToAdd);
	float DistanceToNode(cNode& nodeComparing);

};

#endif // _CNODE_HG_