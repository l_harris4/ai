#ifndef _cMinotaur_HG_
#define _cMinotaur_HG_
#include "cGameObject.h"
#include "cNode.h"

enum MinotaurGuideState
{
	IDLE = 0,
	CHASINGPLAYER = 1,
	HURTINGPLAYER = 2
};

class cMinotaur : public cGameObject
{
public:
	cMinotaur();
	virtual void MakeDecisions();
	MinotaurGuideState state;
	bool moving = true;
	glm::vec3 destination;
	cGameObject* theTarget;
	double lastTimeStep;
	double waitTime;
	double waitTimeReset;
	glm::vec3 objectDestination;
	cNode* currentNode;
	cNode* endingNode;
	int pathCounter;
	std::vector<cNode*> rightPathNodes;
};

#endif



