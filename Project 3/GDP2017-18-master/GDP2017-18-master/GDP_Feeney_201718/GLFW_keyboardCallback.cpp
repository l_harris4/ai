#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"
#include "cMazeMaker.h"

#include <iostream>

bool isShiftKeyDown( int mods, bool bByItself = true );
bool isCtrlKeyDown( int mods, bool bByItself = true );
bool isAltKeyDown( int mods, bool bByItself = true );

extern int g_selectedGameObjectIndex;
extern int g_selectedLightIndex;
extern bool g_movingGameObject;
extern bool g_lightsOn;
extern bool g_texturesOn;
extern cCamera* g_pTheCamera;
extern bool mainObjectMoving;
extern glm::vec3 objectDestination;
extern cMazeMaker* mazeManager;

const float MOVESPEED = 0.6f;
const float CAMERAROTATIONSPEED = 1;
const float CAMERASPEED = 2;
//extern cGameObject* cameraTargetObject;

void updateCamera(glm::vec3 turnChange)
{
	g_pTheCamera->adjustQOrientationFormDeltaEuler(turnChange);

	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->qCameraOrientation) *
		glm::vec4(0, 10, g_pTheCamera->zDist, 0);


	glm::vec3 objectPos;
	objectPos = g_pTheCamera->theObject->position;
	glm::vec3 position = glm::vec3(tempPos.x + objectPos.x
		, tempPos.y + objectPos.y,
		tempPos.z + objectPos.z);

	g_pTheCamera->eye = position;
}




/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

	cGameObject* pLeftTeapot = findObjectByFriendlyName(LEFTTEAPOTNAME, ::g_vecGameObjects);

	switch (key)
	{

	case GLFW_KEY_UP: //Move the camera "forward"
	{
		g_pTheCamera->zDist += 0.5;
		g_pTheCamera->updateTick(0);
		//updateCamera(glm::vec3(0, 0, 0));
	}
	break;
	case GLFW_KEY_DOWN: //Move the camera "backward"
	{
		g_pTheCamera->zDist -= 0.5;
		g_pTheCamera->updateTick(0);
		//updateCamera(glm::vec3(0, 0, 0));
	}
	break;
	case GLFW_KEY_LEFT: //Move the camera "backward"
	{
		updateCamera(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
	}
	break;
	case GLFW_KEY_RIGHT: //Move the camera "backward"
	{
		updateCamera(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
	}
	break;
	case GLFW_KEY_W:
	{
		if (mainObjectMoving == false)
		{
			int rowLoc;
			int columnLoc;
			rowLoc = (int)::g_vecGameObjects[1]->position.x / 2;
			columnLoc = (int)::g_vecGameObjects[1]->position.z / 2;
			columnLoc--;
			if (columnLoc >= 0)
			{
				if (mazeManager->maze[rowLoc][columnLoc][0] == false)
				{
					mainObjectMoving = true;
					objectDestination.x = (rowLoc) * 2;
					objectDestination.z = (columnLoc) * 2;
					objectDestination.y = 2;
					::g_vecGameObjects[1]->vel = glm::normalize(objectDestination - ::g_vecGameObjects[1]->position) * MOVESPEED;
				}
			}
		}
	}
	break;
	case GLFW_KEY_S:
	{
		if (mainObjectMoving == false)
		{
			int rowLoc;
			int columnLoc;
			rowLoc = (int)::g_vecGameObjects[1]->position.x / 2;
			columnLoc = (int)::g_vecGameObjects[1]->position.z / 2;
			columnLoc++;
			if (columnLoc < mazeManager->maze[rowLoc].size())
			{
				if (mazeManager->maze[rowLoc][columnLoc][0] == false)
				{
					mainObjectMoving = true;
					objectDestination.x = (rowLoc) * 2;
					objectDestination.z = (columnLoc) * 2;
					objectDestination.y = 2;
					::g_vecGameObjects[1]->vel = glm::normalize(objectDestination - ::g_vecGameObjects[1]->position) * MOVESPEED;
				}
			}
		}

	}
	break;
	case GLFW_KEY_A:
	{
		if (mainObjectMoving == false)
		{
			int rowLoc;
			int columnLoc;
			rowLoc = (int)::g_vecGameObjects[1]->position.x / 2;
			columnLoc = (int)::g_vecGameObjects[1]->position.z / 2;
			rowLoc--;
			if (rowLoc >= 0)
			{
				if (mazeManager->maze[rowLoc][columnLoc][0] == false)
				{
					mainObjectMoving = true;
					objectDestination.x = (rowLoc) * 2;
					objectDestination.z = (columnLoc) * 2;
					objectDestination.y = 2;
					::g_vecGameObjects[1]->vel = glm::normalize(objectDestination - ::g_vecGameObjects[1]->position) * MOVESPEED;
				}
			}
		}
	}
	break;
	case GLFW_KEY_D:
	{
		if (mainObjectMoving == false)
		{
			int rowLoc;
			int columnLoc;
			rowLoc = (int)::g_vecGameObjects[1]->position.x / 2;
			columnLoc = (int)::g_vecGameObjects[1]->position.z / 2;
			rowLoc++;
			if (rowLoc < mazeManager->maze.size())
			{
				if (mazeManager->maze[rowLoc][columnLoc][0] == false)
				{
					mainObjectMoving = true;
					objectDestination.x = (rowLoc) * 2;
					objectDestination.z = (columnLoc) * 2;
					objectDestination.y = 2;
					::g_vecGameObjects[1]->vel = glm::normalize(objectDestination - ::g_vecGameObjects[1]->position) * MOVESPEED;
				}
			}
		}
	}
	break;


	}
	// HACK: print output to the console
//	std::cout << "Light[0] linear atten: "
//		<< ::g_pLightManager->vecLights[0].attenuation.y << ", "
//		<< ::g_pLightManager->vecLights[0].attenuation.z << std::endl;
	return;
}



// Helper functions
bool isShiftKeyDown( int mods, bool bByItself /*=true*/ )
{
	if ( bByItself )
	{	// shift by itself?
		if ( mods == GLFW_MOD_SHIFT )	{ return true; }
		else							{ return false; }
	}
	else
	{	// shift with anything else, too
		if ( ( mods && GLFW_MOD_SHIFT ) == GLFW_MOD_SHIFT )	{	return true;	}
		else												{	return false;	}
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

//bool isCtrlKeyDown( int mods, bool bByItself = true );
//bool isAltKeyDown( int mods, bool bByItself = true );