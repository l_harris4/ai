#include "cCounterGuide.h"
#include "cGuide.h"
extern int mazeSize;
extern std::vector< cGameObject* >  g_vecGuides;

cCounterGuide::cCounterGuide()
{
	//choose a destination
	destination.x = this->position.x + (rand() % 40 - 20);
	destination.z = this->position.z + (rand() % 40 - 20);
	destination.y = 2;
	if (destination.x < 0)
		destination.x = 0;
	else if (destination.x > mazeSize * 2)
		destination.x = mazeSize * 2;

	if (destination.z < 0)
		destination.z = 0;
	else if (destination.z > mazeSize * 2)
		destination.z = mazeSize * 2;

	state = CounterGuideState::WANDERING;
}

void cCounterGuide::MakeDecisions()
{
	switch (state)
	{
	case CounterGuideState::WANDERING:
	{
		//give velocity towards the destination
		this->vel = glm::normalize(destination - this->position) * 0.4f;
		//if distance to destination is very small, choose a new destination
		if (glm::distance(destination, this->position) < 0.2)
		{
			//choose a destination
			destination.x = this->position.x + (rand() % 40 - 20);
			destination.z = this->position.z + (rand() % 40 - 20);
			destination.y = 2;
			if (destination.x < 0)
				destination.x = 0;
			else if (destination.x > mazeSize * 2)
				destination.x = mazeSize * 2;

			if (destination.z < 0)
				destination.z = 0;
			else if (destination.z > mazeSize * 2)
				destination.z = mazeSize * 2;
		}
		//if distance to a guide is very small then go to chasing mode
		int guidesSize = g_vecGuides.size();
		for (int i = 0; i < guidesSize; ++i)
		{
			if (glm::distance(g_vecGuides[i]->position, this->position) < 5)
			{
				theTarget = g_vecGuides[i];
				state = CounterGuideState::CHASING;
			}
		}
	}
	break;
	case CounterGuideState::CHASING:
	{
		//give velocity towards the guide object
		this->vel = glm::normalize(theTarget->position - this->position) * 0.4f;
		//if basically inside the guide then change its state to dying and our own state to dying
		if (glm::distance(theTarget->position, this->position) < 0.2)
		{
			((cGuide*)theTarget)->state = GuideState::DYING;
			state = CounterGuideState::DEATH;
		}
	}
	break;
	case CounterGuideState::DEATH:
	{
		//just move down for now
		this->vel = glm::vec3(0, -0.4, 0);	
	}
	break;
	}
}
