// Vertex shader
#version 420

uniform mat4 mModel;
uniform mat4 mView;
uniform mat4 mProjection;

// "Vertex" attribute (now use 'in')
in vec4 vCol;		// attribute
in vec3 vPos;		// was: vec2 vPos
in vec3 vNorm;		// Vertex normal
in vec4 uvX2;		// Added: UV 1 and 2

uniform bool isASkyBox;		// Same as the one in the fragments

out vec4 color;				// was: vec4
out vec3 vertNormal;		// Also in "world" (no view or projection)
out vec3 vecWorldPosition;	// 
out vec4 uvX2out;			// Added: UV 1 and 2 to fragment

void main()
{
	vec3 position = vPos;
	
	// Calculate the model view projection matrix here
	mat4 MVP = mProjection * mView * mModel;
	gl_Position = MVP * vec4(position, 1.0f);
	
	// Calculate vertex and normal based on ONLY world 
	vecWorldPosition = vec3( mModel * vec4(position, 1.0f) ).xyz;
	
	// Inv Tran - strips translation and scale from model transform
	// Alternative is you pass a "rotation only" model mat4
	mat4 mWorldInTranspose = inverse(transpose(mModel));
	
	// This normal is in "world space" but only has rotation
	vertNormal = vec3( mWorldInTranspose * vec4(vNorm, 1.0f) ).xyz;		
	
	
    color = vCol;
	uvX2out = uvX2;			// Sent to fragment shader
}


