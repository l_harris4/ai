#ifndef _cGuide_HG_
#define _cGuide_HG_
#include "cGameObject.h"
#include "cNode.h"

enum GuideState
{	
	PURSUEING = 0,
	DEMONSTRATING = 1,
	DYING = 2
};

class cGuide : public cGameObject
{
public:
	cGuide();
	virtual void MakeDecisions();
	GuideState state;
	bool moving = true;

	cNode* currentNode;
	std::vector<cNode*> rightPathNodes;
	glm::vec3 objectDestination;
	int pathCounter;
};

#endif

