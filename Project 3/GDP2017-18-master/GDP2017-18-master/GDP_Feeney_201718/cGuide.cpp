#include "cGuide.h"
#include <map>
#include <iostream>
extern std::vector< cGameObject* >  g_vecGameObjects;
extern std::map<std::pair<std::pair<int, int>, std::pair<int, int>>, std::vector<cNode*>>* allPaths;
extern std::vector<cNode*>* goodNodes;
extern cNode* endingNode;

cGuide::cGuide()
{
	state = GuideState::PURSUEING;
}

void cGuide::MakeDecisions()
{
	//the main object is number 1
	switch (state)
	{
	case GuideState::PURSUEING:
	{
		//update velocity towards the player
		//using persuit
		this->vel = glm::normalize((g_vecGameObjects[1]->position + (g_vecGameObjects[1]->vel*7.0f)) - this->position) * 0.4f;
		this->vel.y = 0;
		//if the distance towards the player is under a certain amount
		if (glm::distance(g_vecGameObjects[1]->position, this->position) < 3)
		{
			//find the current node
			int goodNodesSize = goodNodes->size();
			int xLoc = ((int)g_vecGameObjects[1]->position.x / 2) * 2;
			int zLoc = ((int)g_vecGameObjects[1]->position.z / 2) * 2;
			bool foundNode = false;
			for (int i = 0; i < goodNodesSize; ++i)
			{
				if ((*goodNodes)[i]->xLoc == xLoc && (*goodNodes)[i]->zLoc == zLoc)
				{
					foundNode = true;
					currentNode = (*goodNodes)[i];
					break;
				}
			}

			

			if (foundNode)
			{
				//set the path counter
				//set the vector of paths
				rightPathNodes = (*allPaths)[std::make_pair(std::make_pair(currentNode->xLoc, currentNode->zLoc), std::make_pair(endingNode->xLoc, endingNode->zLoc))];
				currentNode = rightPathNodes[rightPathNodes.size() - 2];
				pathCounter = rightPathNodes.size() - 2;
				//then change the state to demonstrating and change the path
				this->state = GuideState::DEMONSTRATING;
			}
		}
		
	}
	break;
	case GuideState::DEMONSTRATING:
	{
		//set the object destination
		objectDestination.x = currentNode->xLoc;
		objectDestination.z = currentNode->zLoc;
		objectDestination.y = 2;
		//if the main object is moving, move it closer to its destination
		if (moving)
		{
			this->vel = glm::normalize(objectDestination - this->position) * 0.5f;
			if (glm::distance(this->position, objectDestination) <= 0.1f)
			{
				moving = false;
				this->position = objectDestination;
			}
		}
		else if (currentNode != endingNode) //if the object hasnt moved all the way to the end, set the next node to be the current node
		{
			pathCounter--;
			currentNode = rightPathNodes[pathCounter];
			moving = true;
		}
		else
		{
			//set state to persuing again
			this->state = GuideState::PURSUEING;
		}
	}
	break;
	case GuideState::DYING:
	{
		this->vel = glm::vec3(0, -0.4, 0);
	}
	break;
	}
}
