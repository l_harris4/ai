#include "cNode.h"
#include <math.h>
#include <iostream>

//ctor
cNode::cNode()
{
	isValid = false;
	hValue = 0;
	gValue = 0;
	fValue = 0;
}

//ctor with id
cNode::cNode(std::string id)
{
	this->Id = id;
}

//dtor
cNode::~cNode()
{
}

//connects this node to another node
void cNode::AddConnection(cNode& nodeToAdd)
{
	this->connectedNodes.push_back(&nodeToAdd);
}

//testing the distance between this node and another node
float cNode::DistanceToNode(cNode & nodeComparing)
{
	return sqrt(pow((xLoc - nodeComparing.xLoc), 2) + pow((zLoc - nodeComparing.zLoc), 2));
}
