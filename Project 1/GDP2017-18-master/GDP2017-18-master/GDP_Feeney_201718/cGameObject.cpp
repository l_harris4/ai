#include "cGameObject.h"
#include <vector>


// Start the unique IDs at 1. Why not?
/*static*/ unsigned int cGameObject::m_nextUniqueID = 1;

cGameObject::cGameObject()
{
	this->bDiscardTexture = false;
	this->bTransparentTexture = false;
	this->scale = 1.0f;	// (not zero)
	this->position = glm::vec3(0.0f);
	//this->orientation = glm::vec3(0.0f);
	//this->orientation2 = glm::vec3(0.0f);
	this->qOrientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

	this->vel = glm::vec3(0.0f);
	this->accel = glm::vec3(0.0f);	

	// If you aren't sure what the 4th value should be, 
	//	make it a 1.0f ("alpha" or transparency)
	this->diffuseColour = glm::vec4( 0.0f, 0.0f, 0.0f, 1.0f );

	//Assume everything is simulated 
	this->bIsUpdatedInPhysics = true; //??? 
	this->radius = 0.0f;	// Is this the best value??? Who knows?

	this->typeOfObject = eTypeOfObject::UNKNOWN;	// Technically non-standard
	//this->typeOfObject = UNKNOWN;

	this->bIsWireFrame = false;

	// Set all texture blend values to 0.0f (meaning NO texture)
	for (int index = 0; index != NUMTEXTURES; index++)
	{
		this->textureBlend[index] = 0.0f;
	}

	// Assign unque ID, the increment for next created object
	// (Note: if you write your own copy constructor, be sure to COPY this
	//	value, rather than generate a new one - i.e. call the c'tor again)
	this->m_UniqueID = cGameObject::m_nextUniqueID++;

	return;
}

cGameObject::~cGameObject()
{
	return;
}

void cGameObject::overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation)
{
	// Calcualte the quaternion represnetaiton of this Euler axis
	// NOTE: We are OVERWRITING this..
	this->qOrientation = glm::quat(eulerAxisOrientation);


	return;
}

void cGameObject::adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange)
{
	// How do we combine two matrices?
	// That's also how we combine quaternions...

	// So we want to "add" this change in oriention
	glm::quat qRotationChange = glm::quat(eulerAxisOrientChange);

	// Mulitply it by the current orientation;
	this->qOrientation = this->qOrientation * qRotationChange;

	return;
}

void cGameObject::DeleteChildren(void)
{
	for (std::vector< cGameObject* >::iterator itChild = this->vec_pChildObjects.begin();
		itChild != this->vec_pChildObjects.end(); itChild++)
	{
		// Pointer not zero (0)?
		cGameObject* pTempChildObject = (*itChild);
		if (pTempChildObject != 0)
		{
			// Recursively delete all children's children (and so on)
			pTempChildObject->DeleteChildren();
			// Now delete this child
			delete pTempChildObject;
		}
	}
	// There's a vector, but nothing in it
	this->vec_pChildObjects.clear();
	return;
}

cGameObject* cGameObject::FindChildByFriendlyName(std::string name)
{
	for (std::vector<cGameObject*>::iterator itCGO = this->vec_pChildObjects.begin(); itCGO != this->vec_pChildObjects.end(); itCGO++)
	{
		if ((*itCGO)->friendlyName == name)
		{
			return (*itCGO);
		}
	}
	// Didn't find it.
	return NULL;
}

cGameObject* cGameObject::FindChildByID(unsigned int ID)
{
	for (std::vector<cGameObject*>::iterator itCGO = this->vec_pChildObjects.begin(); itCGO != this->vec_pChildObjects.end(); itCGO++)
	{
		if ((*itCGO)->getUniqueID() == ID)
		{
			return (*itCGO);
		}
	}
	// Didn't find it.
	return NULL;
}


//glm::quat cGameObject::getFinalMeshQOrientation(void)
//{
//	return this->m_PhysicalProps.qOrientation * this->m_meshQOrientation;
//}

glm::quat cGameObject::getFinalMeshQOrientation(unsigned int meshID)
{	// Does NOT check for the index of the mesh!
	return this->qOrientation * this->vecMeshes[meshID].getQOrientation();
}

glm::quat cGameObject::getFinalMeshQOrientation(glm::quat &meshQOrientation)
{	// Does NOT check for the index of the mesh!
	return this->qOrientation * meshQOrientation;
}


