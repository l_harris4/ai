The following tests are in the order of appearance in the file TheMain.cpp

//////////////////////////////////////////////////////////////////////////
White box test cases
//////////////////////////////////////////////////////////////////////////
1.
Name: AttachedNodes
Row number of function under test: 352
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: N/A

2.
Name: Distance
Row number of function under test: 366
Row number of test case implementation: N/A
If applicable input parameters and expected output: Creates 2 vec2's representing locations as input
	distance between them as output
How to recover when assert fails: N/A

3.
Name: Path
Row number of function under test: 375
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: If this assert has failed then one must provide a completeable 
	path. The path is determined by nodes listed in ObjectsConfig.txt. I have changed the path to be 
	an unacceptable path. To fix it you will have to go into ObjectsConfig.txt and change the node destination
	on line 53 from "Node Destination: -12,-20" to "Node Destination: 12,20"

4.
Name: FileTextures
Row number of function under test: 380
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: If this assert fails then the MeshConfig file was not loaded correctly,
	this would likely be due simply to wrong placement of the file. Without this file nothing will be rendered,
	so one would have to make sure the MeshConfig file exists and is in the right location.

5.
Name: FileObjects
Row number of function under test: 389
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: If this assert fails then the ObjectsConfig file was not loaded correctly,
	this would likely be due simply to wrong placement of the file. Without this file no objects will be created,
	so one would have to make sure the ObjectsConfig file exists and is in the right location.

6.
Name: PathExists
Row number of function under test: 398
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: N/A

7.
Name: NotEmpty
Row number of function under test: 404
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: N/A

8.
Name: NotEmpty2
Row number of function under test: 408
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: N/A

9.
Name: StartExists
Row number of function under test: 413
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: If this assert has failed then a no node has been found that matches the start location,
	the Node Origin property would have to be changed in the ObjectsConfig.txt file to make it match at least one node location.

10.
Name: EndExists
Row number of function under test: 425
Row number of test case implementation: N/A
If applicable input parameters and expected output: N/A
How to recover when assert fails: If this assert has failed then a no node has been found that matches the end location,
	the Node Destination property would have to be changed in the ObjectsConfig.txt file to make it match at least one node location.


11.
Name: addMyMath
Row number of function under test: 444
Row number of test case implementation: 3
If applicable input parameters and expected output: 1 and 2 as input 3 as output
How to recover when assert fails: This if a function from a dll, if it is broken you are unable to fix it, so the 
	best course of action would be to make your own version of the function and use that instead.

12.
Name: subtractMyMath
Row number of function under test: 450
Row number of test case implementation: 8
If applicable input parameters and expected output: 1 and 2 as input -1 as output
How to recover when assert fails: This if a function from a dll, if it is broken you are unable to fix it, so the 
	best course of action would be to make your own version of the function and use that instead.


13.
Name: multiplyMyMath
Row number of function under test: 456
Row number of test case implementation: 13
If applicable input parameters and expected output: 1 and 2 as input 2 as output
How to recover when assert fails: This if a function from a dll, if it is broken you are unable to fix it, so the 
	best course of action would be to make your own version of the function and use that instead.


14.
Name: divideIntMath
Row number of function under test: 462
Row number of test case implementation: 18
If applicable input parameters and expected output: 1 and 2 as input 0 as output
How to recover when assert fails: This if a function from a dll, if it is broken you are unable to fix it, so the 
	best course of action would be to make your own version of the function and use that instead.


15.
Name: divideFloatMath
Row number of function under test: 468
Row number of test case implementation: 23
If applicable input parameters and expected output: 1.0f and 2.0f as input 0.5f as output
How to recover when assert fails: This if a function from a dll, if it is broken you are unable to fix it, so the 
	best course of action would be to make your own version of the function and use that instead.


16.
Name: equal
Row number of function under test: 474
Row number of test case implementation: 38
If applicable input parameters and expected output: 1 and 1 as input true as output
How to recover when assert fails: N/A

17.
Name: less
Row number of function under test: 480
Row number of test case implementation: 33
If applicable input parameters and expected output: 1 and 2 as input true as output
How to recover when assert fails: N/A

18.
Name: less2
Row number of function under test: 486
Row number of test case implementation: 33
If applicable input parameters and expected output: 1 and 1 as input false as output
How to recover when assert fails: N/A

19.
Name: greater
Row number of function under test: 492
Row number of test case implementation: 28
If applicable input parameters and expected output: 2 and 1 as input true as output
How to recover when assert fails: N/A

20.
Name: greater2
Row number of function under test: 498
Row number of test case implementation: 28
If applicable input parameters and expected output: 1 and 1 as input false as output
How to recover when assert fails: N/A

